"""
Module for the flight_distance function.
"""

from haversine import haversine

def flight_distance(airports_df, airport1: int, airport2: int):
    """
    Calculate the real distance between two airports in kilometers 
    using the information in the datasets from inputs.

    Parameters:
    airports_df (pandas.DataFrame): A DataFrame containing information about airports.
    airport1 (int): The Airport ID of the first airport.
    airport2 (int): The Airport ID of the second airport.

    Returns:
    float: The distance between the two airports in kilometers.

    Notes:
    - The function assumes that the Earth is a sphere and disregards altitude.
    - The function uses the haversine formula to calculate 
      the distance between two points on a sphere.
    - The latitude and longitude of the airports are obtained from the airports_df DataFrame.
    - The haversine function is not defined in the given code snippet.

    Examples:
    >>> airports_df = pd.DataFrame({'Airport ID': ['A1', 'A2'], 
    'Latitude': [40.7128, 37.7749], 
    'Longitude': [-74.0060, -122.4194]})
    >>> flight_distance(airports_df, 'A1', 'A2')
    4137.022560867733

    """
    # Find the two airports based on Airport ID column instead of IATA
    airport1_data = airports_df[airports_df['Airport ID'] == airport1]
    airport2_data = airports_df[airports_df['Airport ID'] == airport2]

    # Check if the data for the airports is ambiguous or not available
    if airport1_data.shape[0] == 0 \
        or airport2_data.shape[0] == 0 or airport1 == '\\N' or airport2 == '\\N':
        return "Couldn't compute (ambiguous data or no data available)"

    # Chooses the first row of the dataframe
    #(in case there are multiple rows with the same airport ID)
    airport1_data = airport1_data.iloc[0]
    airport2_data = airport2_data.iloc[0]

    # Get the latitude and longitude of the airports
    lat1 = airport1_data['Latitude'].item()
    lon1 = airport1_data['Longitude'].item()
    lat2 = airport2_data['Latitude'].item()
    lon2 = airport2_data['Longitude'].item()

    # Normalize points or ensure they are proper lat/lon values
    coords_airport1 = (lat1, lon1)
    coords_airport2 = (lat2, lon2)

    # Calculate the distance between the two airports
    distance = haversine(coords_airport1, coords_airport2)

    # Return the distance in kilometers
    return distance
