"""
Module for testing the flight_distance function.
"""

from haversine import haversine
from Classes.airflight import Airflight
from Functions.flight_distance import flight_distance

class DistanceTest:
    """
    A class to test the flight_distance function.

    Parameters:
    airports_df (pandas.DataFrame): DataFrame containing airport information.

    Methods:
    test_same_airport: Test when the same airport is provided as both inputs.
    test_different_airports_continents: Test the calculation of flight distance 
    between two different airports from different continents.
    test_invalid_airport: Test when an invalid airport ID is provided.

    """

    def __init__(self, airports_df):
        self.airports_df = airports_df

    def test_same_airport(self):
        """
        Test when the same airport is provided as both inputs.
        """
        distance = flight_distance(self.airports_df, 1, 1)
        assert distance == 0.0

    def test_different_airports_continents(self):
        """
        Test the calculation of flight distance between 
        two different airports from different continents.
        """
        distance = flight_distance(self.airports_df, 1, 1638)

        lat_a = self.airports_df[self.airports_df['Airport ID'] == 1]['Latitude'].item()
        lat_b = self.airports_df[self.airports_df['Airport ID'] == 1638]['Latitude'].item()
        lat_c = self.airports_df[self.airports_df['Airport ID'] == 1]['Longitude'].item()
        lat_d = self.airports_df[self.airports_df['Airport ID'] == 1638]['Longitude'].item()

        expected_distance = haversine((lat_a, lat_c), (lat_b, lat_d))
        assert distance == expected_distance

    def test_invalid_airport(self):
        """
        Test when an invalid airport ID is provided.
        """
        # Choose an invalid airport ID (for example, an ID that doesn't exist in the dataset)
        invalid_airport_id = 999999

        # Call the flight_distance function with the invalid airport ID
        distance = flight_distance(self.airports_df, 1, invalid_airport_id)

        # Check if the distance is a string indicating an error message
        assert isinstance(distance, str) and "ambiguous data or no data available" in distance

def main():
    """
    Main function to run tests.
    """
    air_test = Airflight()
    airports_df = air_test.dataframes['airports']

    distance_test = DistanceTest(airports_df)
    distance_test.test_same_airport()
    distance_test.test_different_airports_continents()
    distance_test.test_invalid_airport()

if __name__ == "__main__":
    main()
