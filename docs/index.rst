.. Group 14 Adpro Airflight Data documentation master file, created by
   sphinx-quickstart on Sun Mar 17 12:48:12 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Group 14 Adpro Airflight Data's documentation!
=========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules
   airflight

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
