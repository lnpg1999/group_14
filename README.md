# Group_14

The work present in this gitlab repository was developed by the students for the course of Advanced Programming: 

Francisco Gomes: 39350@novasbe.pt  

Martim Baptista Costa: 39358@novasbe.pt 

Marie Charlotte Jeltsch: 59843@novasbe.pt

Romane Van Marcke De Lummen: 58635@novasbe.pt


## Description
In this project we mainly developed a Class called Airflight in pyhton language. This Airflight class is composed of 8 methods that produce different results, and have different functionalities, regarding data about airports, airplanes, and air routes. The init method of Airflight, when the object is instatiated, automatically fetches the necessary airflight data from the internet. We also had to develop separate functions defined in python files. All things are documented and explained, so it should be easy to understand everything in the code.

## Getting started

After cloning the repository, you will need to manually create a virtual environment using the group_14_project_env.yml file. You can do this with the following command:

> conda env create -f group_14_project_env

This command creates a new environment with the necessary packages, as defined in the group_14_project_env.yml file, that need to be download for the project to properly work.

After creating the environment, you can activate it with:

> conda activate group_14_project_env

## Project Class and Functions documentation

Once the repository of the project is cloned, you can easily acess all the documentation regarding the Class and Functions developed, by going to the docs folder and opening the index.html file.
