"""
Module for the Airflight Class.
"""

import os
import pandas as pd
import zipfile
import geopandas as gpd
import matplotlib.pyplot as plt
from langchain_openai import ChatOpenAI
from IPython.display import Markdown
from Functions.files_download import download_file
from Functions.flight_distance import flight_distance

class Airflight:
    def __init__(self,
        url: str = \
        "https://gitlab.com/adpro1/adpro2024/-/raw/main/Files/flight_data.zip?inline=false",
        output_file: str = '../downloads/fligh_data.zip'):
        """
        Initializes an instance of the Airflight class.

        This method downloads a zip file from a specified URL, extracts its contents,
        and reads the CSV files within the zip file into separate pandas DataFrames.
        The DataFrames are stored as attributes of the class.

        Parameters:
        -----------------------
            url (str): The URL from which to download the zip file. Defaults to
        "https://gitlab.com/adpro1/adpro2024/-/raw/main/Files/flight_data.zip?inline=false".
            output_file (str): The relative path where the downloaded zip file will be saved.
                            Defaults to '../downloads/fligh_data.zip'.
        -----------------------

        Returns:
            None
        -----------------------

        Attributes:
        -----------------------
            dataframes (dict): A dictionary containing pandas DataFrames, where the keys are the
                            names of the CSV files (without the '.csv' extension) and the values
                            are the corresponding DataFrames.
            world (GeoDataFrame): A GeoDataFrame representing a world map, loaded from the
                                'naturalearth_lowres' dataset.
        """

        # Get the current directory
        current_dir = os.getcwd()

        # Create the output file path by joining the current directory with the relative path
        output_file_path = os.path.join(current_dir, output_file)

        # Download the zip file
        download_file(url, output_file_path)

        # Create a ZipFile object to handle the downloaded zip file
        zip_contents = zipfile.ZipFile(output_file_path)

        # Get the list of file names within the zip file
        zip_file_names = zip_contents.namelist()

        # Initialize an empty dictionary to store the DataFrames
        self.dataframes = {}

        # Iterate over each file name in the zip file
        for file_name in zip_file_names:
            # Check if the file is a CSV file
            if file_name.endswith('.csv'):
                # Create a DataFrame name based on the file name
                df_name = file_name.split('.')[0]

                # Print the file name
                print(f"Reading file: {file_name}")

                # Read the CSV file into a DataFrame
                df = pd.read_csv(zip_contents.open(file_name))

                # Store the DataFrame in the dataframes dictionary
                self.dataframes[df_name] = df

        # Load a world map GeoDataFrame
        self.world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))


    def airports_maps(self, country_name):
        """
        The method checks for the existence of an 'airports' 
        DataFrame, filters it for the specified country,
        and plots the airports on a map of the country

        Parameters:
        -----------------------
        country_name: string
            The name of the country for which airports should be plotted

        -----------------------
        Returns:
        None. Displays a plot. If prerequisites are not met 
        (e.g., missing data), prints an error message instead.

        """
        # Ensure the 'airports' DataFrame is available
        if 'airports' not in self.dataframes:
            print("Airports data is not available.")
            return

        # Filter the airports DataFrame for the specified country
        country_airports = self.dataframes['airports']\
            [self.dataframes['airports']['Country'] == country_name]

        # If no airports are found for the country, return an error message
        if country_airports.empty:
            return f"No airports found for {country_name}"

        # Load the map for the specified country
        country_map = self.world[self.world['name'] == country_name]

        # Convert the DataFrame to a GeoDataFrame
        gdf = gpd.GeoDataFrame(
            country_airports, \
                geometry=gpd.points_from_xy(country_airports.Longitude, \
                    country_airports.Latitude))

        # Plot the country map and the airports
        fig, ax = plt.subplots(figsize=(10, 10))
        country_map.plot(ax=ax, color='white', edgecolor='black')
        gdf.plot(ax=ax, marker='o', color='red', markersize=5)
        ax.set_title(f"Airports in {country_name}")
        plt.show()

    def distance_analysis(self):

        """
        This method calculates and plots the distribution 
        of flight distances for all flights
        using the flight_distance function, 
        the 'airports' and 'routes' DataFrames stored as
        attributes of the class.

        Parameters:
        ---------------
            None

        ---------------
        Returns:
            None
        """

        # Get the 'airports' and 'routes' DataFrames from the dataframes dictionary
        airports_df = self.dataframes['airports']
        routes_df = self.dataframes['routes']

        routes_codes = {}

        for index, row in routes_df.iterrows():
            source_airport = row['Source airport ID']
            destination_airport = row['Destination airport ID']

            # Converting to integer the source_airport
            # and destination_airport and running distance function
            if source_airport == '\\N' or destination_airport == '\\N':
                pass
            else:
                source_airport = int(source_airport)
                destination_airport = int(destination_airport)
                distance = flight_distance(airports_df, source_airport, destination_airport)
                # Append to dictionary the index as key
                # and source_airport, destination_airport, distance as values
                routes_codes[index] = [source_airport, destination_airport, distance]

        # Transform routes_codes dictionary into a DataFrame
        routes_codes_df = pd.DataFrame.from_dict(routes_codes, \
                                    orient='index', columns=['Source airport ID', \
                                    'Destination airport ID', 'Distance'])

        # Plot the distribution of flight distances for all flights
        # Drop rows with distance = 'Couldn't compute (ambiguous data or no data available)'
        routes_codes_df = \
            routes_codes_df[routes_codes_df['Distance'] \
                    != "Couldn't compute (ambiguous data or no data available)"]

        print(routes_codes_df.head(5))

        plt.hist(routes_codes_df['Distance'], bins=50, color='blue', edgecolor='black')
        plt.xlabel('Distance (km)')
        plt.ylabel('Frequency')
        plt.title('Distribution of flight distances for all flights')
        plt.show()

    def plot_flights_from_airport(self, airport_iata, internal=False):
        """
        Plot the flight routes departing from a specified airport. Optionally,
        filter the routes to include only internal (domestic) flights.

        This method requires access to two pandas DataFrames: `routes` and `airports`,
        where `routes` contains information about different flight routes, and
        `airports` contains details about airports, including their IATA codes and
        geographical coordinates.

        Parameters
        ----------
        airport_iata : str
            The IATA code of the source airport from which flights are departing.
        internal : bool, optional
            If True, only plots routes that are internal (domestic) to the country
            of the source airport. Default is False, plotting all routes.

        Returns
        -----------
        None
            This method does not return any value. It generates a plot of the flight
            routes departing from the specified airport.
        """
        airports = self.dataframes['airports']
        routes = self.dataframes['routes']

        # Filter the routes dataframe for flights leaving the specified airport
        flights_from_airport = routes[routes["Source airport"] == airport_iata]

        if internal:
            # Find the country of the source airport
            source_country = airports[airports["IATA"] == airport_iata][
                "Country"
            ].values[0]
            # Filter destinations within the same country
            flights_from_airport = flights_from_airport[
                flights_from_airport["Destination airport"].isin(
                    airports[airports["Country"] == source_country]["IATA"]
                )
            ]

        # Retrieve coordinates for plotting
        latitudes = []
        longitudes = []
        for index, row in flights_from_airport.iterrows():
            source_airport_row = airports[airports["IATA"] == row["Source airport"]]
            destination_airport_row = airports[
                airports["IATA"] == row["Destination airport"]
            ]

            if not source_airport_row.empty and not destination_airport_row.empty:
                latitudes.append(
                    [
                        source_airport_row["Latitude"].values[0],
                        destination_airport_row["Latitude"].values[0],
                    ]
                )
                longitudes.append(
                    [
                        source_airport_row["Longitude"].values[0],
                        destination_airport_row["Longitude"].values[0],
                    ]
                )

        # Plotting
        plt.figure(figsize=(10, 6))
        for lat, lon in zip(latitudes, longitudes):
            plt.plot(lon, lat, "-o", markersize=2, linewidth=1)

        plt.xlabel("Longitude")
        plt.ylabel("Latitude")
        plt.title(f"Flights from {airport_iata} (Internal Only: {internal})")
        plt.grid(True)
        plt.show()

    def top_airplane_models_by_routes(self, country=None, N=10):
        """
        Plot the top N most used airplane models based on the number of routes,
        optionally filtered by country or countries.

        This method aggregates route data to find the most frequently used airplane
        models, either globally or within specified country/ies. It then plots a
        bar chart showing the top N airplane models by their usage in routes. The
        method can handle both single and multiple countries as filters.

        Parameters
        ----------
        country : str or list of str, optional
            The country or countries to filter the routes by. If not specified,
            routes worldwide are considered. If a list is provided, it filters
            routes that have airports in any of the listed countries. Default is None.
        N : int, optional
            The number of top airplane models to display. Default is 10.

        Returns
        -------
        None
            This method does not return any value. It generates a bar plot of the
            top N airplane models used in the specified routes.
        """
        airplanes = self.dataframes['airplanes']
        airports = self.dataframes['airports']
        routes = self.dataframes['routes']

        # Filter routes dataframe if country is specified
        if country:
            if isinstance(country, list):
                # If country is a list of countries,
                # filter routes that have airports in these countries
                filtered_airports = airports[airports["Country"].isin(country)]
            else:
                # If country is a single country string,
                # filter routes that have airports in this country
                filtered_airports = airports[airports["Country"] == country]

            # Filtering routes based on the airports in the specified country or countries
            filtered_routes = routes[
                routes["Source airport"].isin(filtered_airports["IATA"])
                | routes["Destination airport"].isin(filtered_airports["IATA"])
            ]
        else:
            # Use all routes if no country is specified
            filtered_routes = routes

        # Counting the occurrences of each airplane model in the filtered dataset
        model_counts = (
            filtered_routes["Equipment"]
            .str.split(" ", expand=True)
            .stack()
            .value_counts()
            .head(N)
        )

        # Filter airplane models to have meaningful
        # names (if available in 'airplanes' dataframe)
        model_names = model_counts.index.map(
            lambda x: airplanes[airplanes["ICAO code"] == x]["Name"].values[0]
            if x in airplanes["ICAO code"].values
            else x
        )

        # Plotting
        plt.figure(figsize=(10, 6))
        plt.bar(model_names, model_counts.values)
        plt.xlabel("Airplane Model")
        plt.ylabel("Number of Routes")
        plt.xticks(rotation=45, ha="right")
        plt.title(
            f'Top {N} Most Used Airplane Models\
            {" Worldwide" if not country else " in " + ", ".join(country)}'
            if isinstance(country, list) else f' in {country}')
        plt.show()

    def plot_routes_by_country(self, country, internal=False, \
                               cutoff_distance=1000, rail_emission_ratio=0.63):

        """
        Plot flight routes for a specified country, 
        distinguishing between short-haul and long-haul flights based on
        the specified cutoff distance. 
        Optionally, filter routes to include only internal (domestic) routes.

        This method filters the global 'airports' 
        and 'routes' data to focus on the specified country. It then
        distinguishes between short-haul and long-haul flights 
        based on the cutoff distance. Short-haul flights are
        those with distances less than or equal to the cutoff distance. 
        It then plots the flight routes on a 2D map,
        using different colors for short-haul and long-haul flights.

        Additionally, the method estimates the potential 
        reduction in flight emissions by replacing short-haul flights
        with rail services, based on the provided rail emission ratio.

        Parameters
        ----------
        country : str
            The name of the country for which flight routes are to be plotted.
        internal : bool, optional
            If True, plots only domestic routes within the specified country. 
            If False, plots all routes originating
            from the country, including international flights. Default is False.
        cutoff_distance : float, optional
            The cutoff distance (in kilometers) 
            to distinguish between short-haul and long-haul flights. Flights with
            distances less than or equal to this cutoff distance are 
            considered short-haul flights. Default is 1000.
        rail_emission_ratio : float, optional
            The ratio of emissions between flights and rail services, 
            typically obtained from credible sources.
            Default is 0.63, indicating a 37% reduction 
            in emissions when switching from flights to rail services
            (Kleinman Center for Energy Policy, University of Pennsylvania).

        Returns
        -------
        None
            This method does not return any values 
            but plots the flight routes on a
            2D map. The 'Total Short-Haul Distance', 
            'Number of Short-Haul Routes' and the 'Estimated Emission Reduction'
            is shown as in the annotation text.
        """

        airports = self.dataframes['airports']
        routes = self.dataframes['routes']

        # Filter airports in the specified country
        country_airports = airports[airports["Country"] == country]
        country_airports_codes = country_airports["Airport ID"].unique()

        for index, row in routes.iterrows():
            source_airport = row['Source airport ID']
            destination_airport = row['Destination airport ID']

            # Converting to integer the source_airport ID and destination_airport ID
            if source_airport == '\\N' or destination_airport == '\\N':
                pass
            else:
                routes.at[index, 'Source airport ID'] = int(source_airport)
                routes.at[index, 'Destination airport ID'] = int(destination_airport)

        # Filter routes where the source airport ID is in the country
        country_routes = routes[routes["Source airport ID"].isin(country_airports_codes)]

        if internal:
            # Further filter for routes where the
            # destination airport is also in the same country
            country_routes = country_routes[
                country_routes["Destination airport ID"].isin(country_airports_codes)
            ]
        else:
            # Include all routes from the country for international flights
            pass

        # Calculate distances for all routes
        country_routes['Distance'] = \
            country_routes.apply(lambda row: \
            flight_distance\
                (airports, row['Source airport ID'], row['Destination airport ID']), axis=1)

        country_routes['Distance'] = pd.to_numeric(country_routes['Distance'], errors='coerce')

        # Distinguish between short-haul and long-haul flights based on the cutoff distance
        short_haul_flights = country_routes[country_routes['Distance'] <= cutoff_distance]
        long_haul_flights = country_routes[country_routes['Distance'] > cutoff_distance]

        # Retrieve coordinates for plotting short-haul flights
        short_haul_latitudes = []
        short_haul_longitudes = []
        for _, row in short_haul_flights.iterrows():
            source_airport_row = airports[
                airports["Airport ID"] == row["Source airport ID"]
            ].iloc[0]
            destination_airport_row = airports[
                airports["Airport ID"] == row["Destination airport ID"]
            ].iloc[0]

            short_haul_latitudes.append(
                [source_airport_row["Latitude"], destination_airport_row["Latitude"]]
            )
            short_haul_longitudes.append(
                [source_airport_row["Longitude"], destination_airport_row["Longitude"]]
            )

        # Retrieve coordinates for plotting long-haul flights
        long_haul_latitudes = []
        long_haul_longitudes = []
        for _, row in long_haul_flights.iterrows():
            source_airport_row = airports[
                airports["Airport ID"] == row["Source airport ID"]
            ].iloc[0]
            destination_airport_row = airports[
                airports["Airport ID"] == row["Destination airport ID"]
            ].iloc[0]

            long_haul_latitudes.append(
                [source_airport_row["Latitude"], destination_airport_row["Latitude"]]
            )
            long_haul_longitudes.append(
                [source_airport_row["Longitude"], destination_airport_row["Longitude"]]
            )

        # Plotting long-haul flights
        plt.figure(figsize=(12, 8))
        for lat, lon in zip(long_haul_latitudes, long_haul_longitudes):
            plt.plot(lon, lat, "-o", markersize=2, linewidth=1, alpha=0.5, color='red')

        # Plotting short-haul flights
        for lat, lon in zip(short_haul_latitudes, short_haul_longitudes):
            plt.plot(lon, lat, "-o", markersize=2, linewidth=1, alpha=0.5, color='blue')

        # Use a set to keep track of unique route pairs
        unique_routes = set()
        total_short_haul_distance = 0

        for _, row in short_haul_flights.iterrows():
            # Ensure unique route pairs
            route_pair = \
                tuple(sorted([row['Source airport ID'], row['Destination airport ID']]))
            if route_pair not in unique_routes:
                unique_routes.add(route_pair)
                total_short_haul_distance += row['Distance']

        # Count the number of short-haul routes
        num_short_haul_routes = len(unique_routes)

        # Calculate the rail emmision reduction
        emission_reduction = rail_emission_ratio * total_short_haul_distance

        # Add annotation with total short-haul distance and number of short-haul routes
        annotation_text = \
            f"Total Short-Haul Distance: {total_short_haul_distance:.2f} km\n" \
            f"Number of Short-Haul Routes: {num_short_haul_routes}\n\n" \
            f"Research Question:\n" \
            f"Estimated Emission Reduction by Replacing Short-Haul Flights with Rail Services: " \
            f"{emission_reduction:.2f} kg CO2"

        # Adjusting the placement of annotation to not overlap with the plots
        plt.annotate(annotation_text, xy=(0.5, -0.15), \
                     xycoords="axes fraction", ha="center", fontsize=10, va='top')
        plt.xlabel("Longitude")
        plt.ylabel("Latitude")
        plt.title(f"Flight Routes from {country} (Short-haul: Blue, Long-haul: Red)")
        plt.grid(True)
        plt.show()

    def aircrafts(self):
        """
        Print the list of aircraft models from the airplanes DataFrame column 'Name'.

        Parameters
        ----------
        None

        Returns
        -------
        None
            This method does not return any value.
            It prints the list of aircraft models.
        """
        airplanes = self.dataframes['airplanes']
        print('List of aircraft models:')
        print('------------------------')
        print(airplanes['Name'].to_string(index=False))

    def airport_info(self, airport_name: str):
        """
        Retrieves and formats detailed specifications of 
        a specified airport using a Language Learning Model (LLM).

        This method queries the configured LLM to generate 
        a Markdown table containing specifications for the airport.
        The airport name is provided as a parameter to the method. 
        The LLM is then invoked to retrieve the detailed
        specifications of the airport. 
        The specifications are formatted into a 
        table with two columns: the first column
        represents the specification and the second column 
        represents the corresponding value.

        Parameters:
        -----------
        airport_name : str
        The name of the airport for which specifications are requested.

        Returns:
        --------
        None
            This method does not return any value.
            It prints the formatted Markdown table 
            containing the specifications of the chosen airport.
        """
        llm = ChatOpenAI(temperature=0.1)

        output = llm.invoke('Give me a table with the detailed \
                            specifications of the ' + airport_name + \
                            ' airport. No introduction sentence or conclusion, \
                            just the name of the airport and \
                            its specifications/characteristics \
                            in a table. A column for spec. \
                            Table format: 2 columns, 1st column: \
                            specification, 2nd column: value.')

        return Markdown(output.content)

    def aircraft_info(self, aircraft_name: str):

        """
            Retrieves and formats detailed specifications 
            of a specified aircraft model using a Language Learning Model (LLM).
            The specifications are printed in Markdown 
            format directly to the console or to a Jupyter notebook cell output.

            This method searches the 'airplanes' DataFrame 
            to find an aircraft that matches the provided name. If a match is found, 
            it queries the configured LLM to generate a Markdown 
            table containing specifications for the aircraft. 
            If the specified aircraft cannot be located within the dataset, 
            the method raises a ValueError. This error provides a list of valid
            aircraft names from the dataset to assist the user in making a valid selection.


            Parameters:
            ------------
            aircraft_name : str
                The name (or a part of the name) of the aircraft for 
                which specifications are requested. This search is
                case-insensitive.

            Raises:
            ------------
            Exception:
                - If the 'airplanes' DataFrame is unavailable or not loaded 
                into the class instance, indicating that the aircraft
                data is inaccessible.
                - If the LLM is not initialized properly, 
                signaling an issue with the OPENAI_API_KEY or its setup.
            
            ValueError:
                Raised if the specified aircraft 
                name does not match any aircraft within the 'airplanes' DataFrame.
                This error message includes a list of available 
                aircraft names from the dataset to assist in selecting a
                valid name.

            Returns:
            ------------
                None
                This method does not return any value.
                It prints the formatted Markdown table 
                containing the specifications of the chosen aircraft.
            """

        # Initialize the LLM
        llm = ChatOpenAI(temperature=0.1)

        # Ensure 'airplanes' DataFrame is available
        if "airplanes" not in self.dataframes:
            raise Exception("Aircraft data is not available. \
                            Please ensure the 'airplanes' DataFrame is loaded.")

        # Filter the 'airplanes' DataFrame for the specified aircraft_name
        aircraft_df = self.dataframes["airplanes"]
        aircraft_data = aircraft_df[aircraft_df["Name"].\
                                    str.contains(aircraft_name, case=False)]

        # If no aircraft is found, inform the user and suggest valid aircraft names
        if aircraft_data.empty:
            available_aircrafts = aircraft_df["Name"].unique().tolist()
            # Include all available aircraft names in the suggestions
            suggestions = "\n- " + "\n- ".join(available_aircrafts)
            error_message = (
                f"No aircraft found with the name '{aircraft_name}'.\n\n"
                "Please choose one of the available options:\n"
                f"{suggestions}\n"
            )
            raise ValueError(error_message)


        # Use the SmartDataframe's chat method with the configured LLM

        query = f"Provide a table with specifications \
            for the aircraft named {aircraft_name}. \
            Just specifications without showing intro or conclusion. \
            Table format: 2 columns, 1st column: \
            specification, 2nd column: value."
        response = llm.invoke(query)

        return Markdown(response.content)
